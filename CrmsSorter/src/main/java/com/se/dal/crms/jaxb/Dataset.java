//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.11 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2021.01.25 à 05:53:10 PM CET 
//


package com.se.dal.crms.jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="characteristics"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="cstic" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                           &lt;attribute name="cstic_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="cformat" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="len" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="node_struct" type="{}node_struct"/&gt;
 *         &lt;element name="dm_attribute"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="cstic_info" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                           &lt;attribute name="dm_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="tabular" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="cstic_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="pos" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *                           &lt;attribute name="col" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *                           &lt;attribute name="crms_use" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="dm_valuation" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="cref_valuation" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="cstic_value" maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;simpleContent&gt;
 *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                                     &lt;attribute name="tabular" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                     &lt;attribute name="cstic_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                     &lt;attribute name="val_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                     &lt;attribute name="Col" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *                                     &lt;attribute name="Row" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *                                   &lt;/extension&gt;
 *                                 &lt;/simpleContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="cref" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="dm_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="renditions"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="mat_rendition" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="desc"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;simpleContent&gt;
 *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                                     &lt;attribute name="lang" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                     &lt;attribute name="desc30" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                   &lt;/extension&gt;
 *                                 &lt;/simpleContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="mat_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="cstic_rendition" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="desc"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;simpleContent&gt;
 *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                                     &lt;attribute name="lang" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                     &lt;attribute name="desc30" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                   &lt;/extension&gt;
 *                                 &lt;/simpleContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="cstic_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="cstic_value_rendition" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="desc"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;simpleContent&gt;
 *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                                     &lt;attribute name="lang" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                     &lt;attribute name="desc30" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                   &lt;/extension&gt;
 *                                 &lt;/simpleContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="cstic_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="val_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="date" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="time" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="mode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "characteristics",
    "nodeStruct",
    "dmAttribute",
    "dmValuation",
    "renditions"
})
@XmlRootElement(name = "dataset")
public class Dataset {

    @XmlElement(required = true)
    protected Dataset.Characteristics characteristics;
    @XmlElement(name = "node_struct", required = true)
    protected NodeStruct nodeStruct;
    @XmlElement(name = "dm_attribute", required = true)
    protected Dataset.DmAttribute dmAttribute;
    @XmlElement(name = "dm_valuation")
    protected List<Dataset.DmValuation> dmValuation;
    @XmlElement(required = true)
    protected Dataset.Renditions renditions;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "date")
    protected String date;
    @XmlAttribute(name = "time")
    protected String time;
    @XmlAttribute(name = "mode")
    protected String mode;

    /**
     * Obtient la valeur de la propriété characteristics.
     * 
     * @return
     *     possible object is
     *     {@link Dataset.Characteristics }
     *     
     */
    public Dataset.Characteristics getCharacteristics() {
        return characteristics;
    }

    /**
     * Définit la valeur de la propriété characteristics.
     * 
     * @param value
     *     allowed object is
     *     {@link Dataset.Characteristics }
     *     
     */
    public void setCharacteristics(Dataset.Characteristics value) {
        this.characteristics = value;
    }

    /**
     * Obtient la valeur de la propriété nodeStruct.
     * 
     * @return
     *     possible object is
     *     {@link NodeStruct }
     *     
     */
    public NodeStruct getNodeStruct() {
        return nodeStruct;
    }

    /**
     * Définit la valeur de la propriété nodeStruct.
     * 
     * @param value
     *     allowed object is
     *     {@link NodeStruct }
     *     
     */
    public void setNodeStruct(NodeStruct value) {
        this.nodeStruct = value;
    }

    /**
     * Obtient la valeur de la propriété dmAttribute.
     * 
     * @return
     *     possible object is
     *     {@link Dataset.DmAttribute }
     *     
     */
    public Dataset.DmAttribute getDmAttribute() {
        return dmAttribute;
    }

    /**
     * Définit la valeur de la propriété dmAttribute.
     * 
     * @param value
     *     allowed object is
     *     {@link Dataset.DmAttribute }
     *     
     */
    public void setDmAttribute(Dataset.DmAttribute value) {
        this.dmAttribute = value;
    }

    /**
     * Gets the value of the dmValuation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dmValuation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDmValuation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Dataset.DmValuation }
     * 
     * 
     */
    public List<Dataset.DmValuation> getDmValuation() {
        if (dmValuation == null) {
            dmValuation = new ArrayList<Dataset.DmValuation>();
        }
        return this.dmValuation;
    }

    /**
     * Obtient la valeur de la propriété renditions.
     * 
     * @return
     *     possible object is
     *     {@link Dataset.Renditions }
     *     
     */
    public Dataset.Renditions getRenditions() {
        return renditions;
    }

    /**
     * Définit la valeur de la propriété renditions.
     * 
     * @param value
     *     allowed object is
     *     {@link Dataset.Renditions }
     *     
     */
    public void setRenditions(Dataset.Renditions value) {
        this.renditions = value;
    }

    /**
     * Obtient la valeur de la propriété name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Définit la valeur de la propriété name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Obtient la valeur de la propriété date.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDate() {
        return date;
    }

    /**
     * Définit la valeur de la propriété date.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDate(String value) {
        this.date = value;
    }

    /**
     * Obtient la valeur de la propriété time.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTime() {
        return time;
    }

    /**
     * Définit la valeur de la propriété time.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTime(String value) {
        this.time = value;
    }

    /**
     * Obtient la valeur de la propriété mode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMode() {
        return mode;
    }

    /**
     * Définit la valeur de la propriété mode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMode(String value) {
        this.mode = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="cstic" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *                 &lt;attribute name="cstic_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="cformat" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="len" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cstic"
    })
    public static class Characteristics {

        protected List<Dataset.Characteristics.Cstic> cstic;

        /**
         * Gets the value of the cstic property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the cstic property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCstic().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Dataset.Characteristics.Cstic }
         * 
         * 
         */
        public List<Dataset.Characteristics.Cstic> getCstic() {
            if (cstic == null) {
                cstic = new ArrayList<Dataset.Characteristics.Cstic>();
            }
            return this.cstic;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         * 
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *       &lt;attribute name="cstic_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="cformat" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="len" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class Cstic {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "cstic_id")
            protected String csticId;
            @XmlAttribute(name = "type")
            protected String type;
            @XmlAttribute(name = "cformat")
            protected String cformat;
            @XmlAttribute(name = "len")
            protected Integer len;

            /**
             * Obtient la valeur de la propriété value.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Définit la valeur de la propriété value.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Obtient la valeur de la propriété csticId.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCsticId() {
                return csticId;
            }

            /**
             * Définit la valeur de la propriété csticId.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCsticId(String value) {
                this.csticId = value;
            }

            /**
             * Obtient la valeur de la propriété type.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getType() {
                return type;
            }

            /**
             * Définit la valeur de la propriété type.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setType(String value) {
                this.type = value;
            }

            /**
             * Obtient la valeur de la propriété cformat.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCformat() {
                return cformat;
            }

            /**
             * Définit la valeur de la propriété cformat.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCformat(String value) {
                this.cformat = value;
            }

            /**
             * Obtient la valeur de la propriété len.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getLen() {
                return len;
            }

            /**
             * Définit la valeur de la propriété len.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setLen(Integer value) {
                this.len = value;
            }

        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="cstic_info" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *                 &lt;attribute name="dm_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="tabular" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="cstic_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="pos" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
     *                 &lt;attribute name="col" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
     *                 &lt;attribute name="crms_use" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "csticInfo"
    })
    public static class DmAttribute {

        @XmlElement(name = "cstic_info")
        protected List<Dataset.DmAttribute.CsticInfo> csticInfo;

        /**
         * Gets the value of the csticInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the csticInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCsticInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Dataset.DmAttribute.CsticInfo }
         * 
         * 
         */
        public List<Dataset.DmAttribute.CsticInfo> getCsticInfo() {
            if (csticInfo == null) {
                csticInfo = new ArrayList<Dataset.DmAttribute.CsticInfo>();
            }
            return this.csticInfo;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         * 
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *       &lt;attribute name="dm_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="tabular" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="cstic_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="pos" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
         *       &lt;attribute name="col" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
         *       &lt;attribute name="crms_use" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class CsticInfo {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "dm_id")
            protected String dmId;
            @XmlAttribute(name = "tabular")
            protected String tabular;
            @XmlAttribute(name = "cstic_id")
            protected String csticId;
            @XmlAttribute(name = "pos")
            protected Integer pos;
            @XmlAttribute(name = "col")
            protected Integer col;
            @XmlAttribute(name = "crms_use")
            protected String crmsUse;

            /**
             * Obtient la valeur de la propriété value.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Définit la valeur de la propriété value.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Obtient la valeur de la propriété dmId.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDmId() {
                return dmId;
            }

            /**
             * Définit la valeur de la propriété dmId.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDmId(String value) {
                this.dmId = value;
            }

            /**
             * Obtient la valeur de la propriété tabular.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTabular() {
                return tabular;
            }

            /**
             * Définit la valeur de la propriété tabular.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTabular(String value) {
                this.tabular = value;
            }

            /**
             * Obtient la valeur de la propriété csticId.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCsticId() {
                return csticId;
            }

            /**
             * Définit la valeur de la propriété csticId.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCsticId(String value) {
                this.csticId = value;
            }

            /**
             * Obtient la valeur de la propriété pos.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getPos() {
                return pos;
            }

            /**
             * Définit la valeur de la propriété pos.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setPos(Integer value) {
                this.pos = value;
            }

            /**
             * Obtient la valeur de la propriété col.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getCol() {
                return col;
            }

            /**
             * Définit la valeur de la propriété col.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setCol(Integer value) {
                this.col = value;
            }

            /**
             * Obtient la valeur de la propriété crmsUse.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCrmsUse() {
                return crmsUse;
            }

            /**
             * Définit la valeur de la propriété crmsUse.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCrmsUse(String value) {
                this.crmsUse = value;
            }

        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="cref_valuation" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="cstic_value" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;simpleContent&gt;
     *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *                           &lt;attribute name="tabular" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                           &lt;attribute name="cstic_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                           &lt;attribute name="val_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                           &lt;attribute name="Col" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
     *                           &lt;attribute name="Row" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
     *                         &lt;/extension&gt;
     *                       &lt;/simpleContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="cref" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="dm_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "crefValuation"
    })
    public static class DmValuation {

        @XmlElement(name = "cref_valuation")
        protected List<Dataset.DmValuation.CrefValuation> crefValuation;
        @XmlAttribute(name = "dm_id")
        protected String dmId;

        /**
         * Gets the value of the crefValuation property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the crefValuation property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCrefValuation().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Dataset.DmValuation.CrefValuation }
         * 
         * 
         */
        public List<Dataset.DmValuation.CrefValuation> getCrefValuation() {
            if (crefValuation == null) {
                crefValuation = new ArrayList<Dataset.DmValuation.CrefValuation>();
            }
            return this.crefValuation;
        }

        /**
         * Obtient la valeur de la propriété dmId.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDmId() {
            return dmId;
        }

        /**
         * Définit la valeur de la propriété dmId.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDmId(String value) {
            this.dmId = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         * 
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="cstic_value" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;simpleContent&gt;
         *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *                 &lt;attribute name="tabular" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                 &lt;attribute name="cstic_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                 &lt;attribute name="val_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                 &lt;attribute name="Col" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
         *                 &lt;attribute name="Row" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
         *               &lt;/extension&gt;
         *             &lt;/simpleContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="cref" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "csticValue"
        })
        public static class CrefValuation {

            @XmlElement(name = "cstic_value")
            protected List<Dataset.DmValuation.CrefValuation.CsticValue> csticValue;
            @XmlAttribute(name = "cref")
            protected String cref;

            /**
             * Gets the value of the csticValue property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the csticValue property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCsticValue().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Dataset.DmValuation.CrefValuation.CsticValue }
             * 
             * 
             */
            public List<Dataset.DmValuation.CrefValuation.CsticValue> getCsticValue() {
                if (csticValue == null) {
                    csticValue = new ArrayList<Dataset.DmValuation.CrefValuation.CsticValue>();
                }
                return this.csticValue;
            }

            /**
             * Obtient la valeur de la propriété cref.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCref() {
                return cref;
            }

            /**
             * Définit la valeur de la propriété cref.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCref(String value) {
                this.cref = value;
            }


            /**
             * <p>Classe Java pour anonymous complex type.
             * 
             * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;simpleContent&gt;
             *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
             *       &lt;attribute name="tabular" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *       &lt;attribute name="cstic_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *       &lt;attribute name="val_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *       &lt;attribute name="Col" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
             *       &lt;attribute name="Row" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
             *     &lt;/extension&gt;
             *   &lt;/simpleContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class CsticValue {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "tabular")
                protected String tabular;
                @XmlAttribute(name = "cstic_id")
                protected String csticId;
                @XmlAttribute(name = "val_id")
                protected String valId;
                @XmlAttribute(name = "Col")
                protected Integer col;
                @XmlAttribute(name = "Row")
                protected Integer row;

                /**
                 * Obtient la valeur de la propriété value.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * Définit la valeur de la propriété value.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

                /**
                 * Obtient la valeur de la propriété tabular.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTabular() {
                    return tabular;
                }

                /**
                 * Définit la valeur de la propriété tabular.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTabular(String value) {
                    this.tabular = value;
                }

                /**
                 * Obtient la valeur de la propriété csticId.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCsticId() {
                    return csticId;
                }

                /**
                 * Définit la valeur de la propriété csticId.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCsticId(String value) {
                    this.csticId = value;
                }

                /**
                 * Obtient la valeur de la propriété valId.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValId() {
                    return valId;
                }

                /**
                 * Définit la valeur de la propriété valId.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValId(String value) {
                    this.valId = value;
                }

                /**
                 * Obtient la valeur de la propriété col.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getCol() {
                    return col;
                }

                /**
                 * Définit la valeur de la propriété col.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setCol(Integer value) {
                    this.col = value;
                }

                /**
                 * Obtient la valeur de la propriété row.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getRow() {
                    return row;
                }

                /**
                 * Définit la valeur de la propriété row.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setRow(Integer value) {
                    this.row = value;
                }

            }

        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="mat_rendition" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="desc"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;simpleContent&gt;
     *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *                           &lt;attribute name="lang" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                           &lt;attribute name="desc30" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                         &lt;/extension&gt;
     *                       &lt;/simpleContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="mat_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="cstic_rendition" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="desc"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;simpleContent&gt;
     *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *                           &lt;attribute name="lang" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                           &lt;attribute name="desc30" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                         &lt;/extension&gt;
     *                       &lt;/simpleContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="cstic_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="cstic_value_rendition" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="desc"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;simpleContent&gt;
     *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *                           &lt;attribute name="lang" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                           &lt;attribute name="desc30" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                         &lt;/extension&gt;
     *                       &lt;/simpleContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="cstic_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="val_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "matRendition",
        "csticRendition",
        "csticValueRendition"
    })
    public static class Renditions {

        @XmlElement(name = "mat_rendition")
        protected List<Dataset.Renditions.MatRendition> matRendition;
        @XmlElement(name = "cstic_rendition")
        protected List<Dataset.Renditions.CsticRendition> csticRendition;
        @XmlElement(name = "cstic_value_rendition")
        protected List<Dataset.Renditions.CsticValueRendition> csticValueRendition;

        /**
         * Gets the value of the matRendition property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the matRendition property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMatRendition().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Dataset.Renditions.MatRendition }
         * 
         * 
         */
        public List<Dataset.Renditions.MatRendition> getMatRendition() {
            if (matRendition == null) {
                matRendition = new ArrayList<Dataset.Renditions.MatRendition>();
            }
            return this.matRendition;
        }

        /**
         * Gets the value of the csticRendition property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the csticRendition property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCsticRendition().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Dataset.Renditions.CsticRendition }
         * 
         * 
         */
        public List<Dataset.Renditions.CsticRendition> getCsticRendition() {
            if (csticRendition == null) {
                csticRendition = new ArrayList<Dataset.Renditions.CsticRendition>();
            }
            return this.csticRendition;
        }

        /**
         * Gets the value of the csticValueRendition property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the csticValueRendition property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCsticValueRendition().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Dataset.Renditions.CsticValueRendition }
         * 
         * 
         */
        public List<Dataset.Renditions.CsticValueRendition> getCsticValueRendition() {
            if (csticValueRendition == null) {
                csticValueRendition = new ArrayList<Dataset.Renditions.CsticValueRendition>();
            }
            return this.csticValueRendition;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         * 
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="desc"&gt;
         *           &lt;complexType&gt;
         *             &lt;simpleContent&gt;
         *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *                 &lt;attribute name="lang" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                 &lt;attribute name="desc30" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *               &lt;/extension&gt;
         *             &lt;/simpleContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="cstic_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "desc"
        })
        public static class CsticRendition {

            @XmlElement(required = true)
            protected Dataset.Renditions.CsticRendition.Desc desc;
            @XmlAttribute(name = "cstic_id")
            protected String csticId;

            /**
             * Obtient la valeur de la propriété desc.
             * 
             * @return
             *     possible object is
             *     {@link Dataset.Renditions.CsticRendition.Desc }
             *     
             */
            public Dataset.Renditions.CsticRendition.Desc getDesc() {
                return desc;
            }

            /**
             * Définit la valeur de la propriété desc.
             * 
             * @param value
             *     allowed object is
             *     {@link Dataset.Renditions.CsticRendition.Desc }
             *     
             */
            public void setDesc(Dataset.Renditions.CsticRendition.Desc value) {
                this.desc = value;
            }

            /**
             * Obtient la valeur de la propriété csticId.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCsticId() {
                return csticId;
            }

            /**
             * Définit la valeur de la propriété csticId.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCsticId(String value) {
                this.csticId = value;
            }


            /**
             * <p>Classe Java pour anonymous complex type.
             * 
             * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;simpleContent&gt;
             *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
             *       &lt;attribute name="lang" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *       &lt;attribute name="desc30" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *     &lt;/extension&gt;
             *   &lt;/simpleContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class Desc {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "lang")
                protected String lang;
                @XmlAttribute(name = "desc30")
                protected String desc30;

                /**
                 * Obtient la valeur de la propriété value.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * Définit la valeur de la propriété value.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

                /**
                 * Obtient la valeur de la propriété lang.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLang() {
                    return lang;
                }

                /**
                 * Définit la valeur de la propriété lang.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLang(String value) {
                    this.lang = value;
                }

                /**
                 * Obtient la valeur de la propriété desc30.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDesc30() {
                    return desc30;
                }

                /**
                 * Définit la valeur de la propriété desc30.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDesc30(String value) {
                    this.desc30 = value;
                }

            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         * 
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="desc"&gt;
         *           &lt;complexType&gt;
         *             &lt;simpleContent&gt;
         *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *                 &lt;attribute name="lang" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                 &lt;attribute name="desc30" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *               &lt;/extension&gt;
         *             &lt;/simpleContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="cstic_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="val_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "desc"
        })
        public static class CsticValueRendition {

            @XmlElement(required = true)
            protected Dataset.Renditions.CsticValueRendition.Desc desc;
            @XmlAttribute(name = "cstic_id")
            protected String csticId;
            @XmlAttribute(name = "val_id")
            protected String valId;

            /**
             * Obtient la valeur de la propriété desc.
             * 
             * @return
             *     possible object is
             *     {@link Dataset.Renditions.CsticValueRendition.Desc }
             *     
             */
            public Dataset.Renditions.CsticValueRendition.Desc getDesc() {
                return desc;
            }

            /**
             * Définit la valeur de la propriété desc.
             * 
             * @param value
             *     allowed object is
             *     {@link Dataset.Renditions.CsticValueRendition.Desc }
             *     
             */
            public void setDesc(Dataset.Renditions.CsticValueRendition.Desc value) {
                this.desc = value;
            }

            /**
             * Obtient la valeur de la propriété csticId.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCsticId() {
                return csticId;
            }

            /**
             * Définit la valeur de la propriété csticId.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCsticId(String value) {
                this.csticId = value;
            }

            /**
             * Obtient la valeur de la propriété valId.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValId() {
                return valId;
            }

            /**
             * Définit la valeur de la propriété valId.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValId(String value) {
                this.valId = value;
            }


            /**
             * <p>Classe Java pour anonymous complex type.
             * 
             * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;simpleContent&gt;
             *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
             *       &lt;attribute name="lang" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *       &lt;attribute name="desc30" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *     &lt;/extension&gt;
             *   &lt;/simpleContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class Desc {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "lang")
                protected String lang;
                @XmlAttribute(name = "desc30")
                protected String desc30;

                /**
                 * Obtient la valeur de la propriété value.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * Définit la valeur de la propriété value.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

                /**
                 * Obtient la valeur de la propriété lang.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLang() {
                    return lang;
                }

                /**
                 * Définit la valeur de la propriété lang.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLang(String value) {
                    this.lang = value;
                }

                /**
                 * Obtient la valeur de la propriété desc30.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDesc30() {
                    return desc30;
                }

                /**
                 * Définit la valeur de la propriété desc30.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDesc30(String value) {
                    this.desc30 = value;
                }

            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         * 
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="desc"&gt;
         *           &lt;complexType&gt;
         *             &lt;simpleContent&gt;
         *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *                 &lt;attribute name="lang" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                 &lt;attribute name="desc30" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *               &lt;/extension&gt;
         *             &lt;/simpleContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="mat_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "desc"
        })
        public static class MatRendition {

            @XmlElement(required = true)
            protected Dataset.Renditions.MatRendition.Desc desc;
            @XmlAttribute(name = "mat_id")
            protected String matId;

            /**
             * Obtient la valeur de la propriété desc.
             * 
             * @return
             *     possible object is
             *     {@link Dataset.Renditions.MatRendition.Desc }
             *     
             */
            public Dataset.Renditions.MatRendition.Desc getDesc() {
                return desc;
            }

            /**
             * Définit la valeur de la propriété desc.
             * 
             * @param value
             *     allowed object is
             *     {@link Dataset.Renditions.MatRendition.Desc }
             *     
             */
            public void setDesc(Dataset.Renditions.MatRendition.Desc value) {
                this.desc = value;
            }

            /**
             * Obtient la valeur de la propriété matId.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMatId() {
                return matId;
            }

            /**
             * Définit la valeur de la propriété matId.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMatId(String value) {
                this.matId = value;
            }


            /**
             * <p>Classe Java pour anonymous complex type.
             * 
             * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;simpleContent&gt;
             *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
             *       &lt;attribute name="lang" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *       &lt;attribute name="desc30" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *     &lt;/extension&gt;
             *   &lt;/simpleContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class Desc {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "lang")
                protected String lang;
                @XmlAttribute(name = "desc30")
                protected String desc30;

                /**
                 * Obtient la valeur de la propriété value.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * Définit la valeur de la propriété value.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

                /**
                 * Obtient la valeur de la propriété lang.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLang() {
                    return lang;
                }

                /**
                 * Définit la valeur de la propriété lang.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLang(String value) {
                    this.lang = value;
                }

                /**
                 * Obtient la valeur de la propriété desc30.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDesc30() {
                    return desc30;
                }

                /**
                 * Définit la valeur de la propriété desc30.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDesc30(String value) {
                    this.desc30 = value;
                }

            }

        }

    }

}
