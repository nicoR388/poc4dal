package com.se.dal.crms;

import java.io.File;

public class SorterCrmsMain {

	public static void main(String[] args) throws Exception {
		String generatedFile = "sorted_crms_standard.xml";
		if(args == null || args.length == 0) {
			throw new IllegalArgumentException("You must set the path for xml file");
		}
		if(args.length > 1) {
			throw new IllegalArgumentException("Expected only one argument");
		}
		if (args.length == 1) {
			String filePath = args[0];
			if(!args[0].contains(File.separator)) {
				filePath = "." + File.separator + args[0];
			}
			File file = new File(filePath);
			SortXMLAlphabetically xmlSorter = new SortXMLAlphabetically(file, generatedFile);
			xmlSorter.sort();
		}
	}
}
