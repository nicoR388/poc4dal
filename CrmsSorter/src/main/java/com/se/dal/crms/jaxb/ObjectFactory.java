//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.11 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2021.01.25 à 05:53:10 PM CET 
//


package com.se.dal.crms.jaxb;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.se.pim.dal.export.crms.standard.jaxb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.se.pim.dal.export.crms.standard.jaxb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Dataset }
     * 
     */
    public Dataset createDataset() {
        return new Dataset();
    }

    /**
     * Create an instance of {@link Dataset.Renditions }
     * 
     */
    public Dataset.Renditions createDatasetRenditions() {
        return new Dataset.Renditions();
    }

    /**
     * Create an instance of {@link Dataset.Renditions.CsticValueRendition }
     * 
     */
    public Dataset.Renditions.CsticValueRendition createDatasetRenditionsCsticValueRendition() {
        return new Dataset.Renditions.CsticValueRendition();
    }

    /**
     * Create an instance of {@link Dataset.Renditions.CsticRendition }
     * 
     */
    public Dataset.Renditions.CsticRendition createDatasetRenditionsCsticRendition() {
        return new Dataset.Renditions.CsticRendition();
    }

    /**
     * Create an instance of {@link Dataset.Renditions.MatRendition }
     * 
     */
    public Dataset.Renditions.MatRendition createDatasetRenditionsMatRendition() {
        return new Dataset.Renditions.MatRendition();
    }

    /**
     * Create an instance of {@link Dataset.DmValuation }
     * 
     */
    public Dataset.DmValuation createDatasetDmValuation() {
        return new Dataset.DmValuation();
    }

    /**
     * Create an instance of {@link Dataset.DmValuation.CrefValuation }
     * 
     */
    public Dataset.DmValuation.CrefValuation createDatasetDmValuationCrefValuation() {
        return new Dataset.DmValuation.CrefValuation();
    }

    /**
     * Create an instance of {@link Dataset.DmAttribute }
     * 
     */
    public Dataset.DmAttribute createDatasetDmAttribute() {
        return new Dataset.DmAttribute();
    }

    /**
     * Create an instance of {@link NodeStruct }
     * 
     */
    public NodeStruct createNodeStruct() {
        return new NodeStruct();
    }

    /**
     * Create an instance of {@link Dataset.Characteristics }
     * 
     */
    public Dataset.Characteristics createDatasetCharacteristics() {
        return new Dataset.Characteristics();
    }

    /**
     * Create an instance of {@link Dataset.Renditions.CsticValueRendition.Desc }
     * 
     */
    public Dataset.Renditions.CsticValueRendition.Desc createDatasetRenditionsCsticValueRenditionDesc() {
        return new Dataset.Renditions.CsticValueRendition.Desc();
    }

    /**
     * Create an instance of {@link Dataset.Renditions.CsticRendition.Desc }
     * 
     */
    public Dataset.Renditions.CsticRendition.Desc createDatasetRenditionsCsticRenditionDesc() {
        return new Dataset.Renditions.CsticRendition.Desc();
    }

    /**
     * Create an instance of {@link Dataset.Renditions.MatRendition.Desc }
     * 
     */
    public Dataset.Renditions.MatRendition.Desc createDatasetRenditionsMatRenditionDesc() {
        return new Dataset.Renditions.MatRendition.Desc();
    }

    /**
     * Create an instance of {@link Dataset.DmValuation.CrefValuation.CsticValue }
     * 
     */
    public Dataset.DmValuation.CrefValuation.CsticValue createDatasetDmValuationCrefValuationCsticValue() {
        return new Dataset.DmValuation.CrefValuation.CsticValue();
    }

    /**
     * Create an instance of {@link Dataset.DmAttribute.CsticInfo }
     * 
     */
    public Dataset.DmAttribute.CsticInfo createDatasetDmAttributeCsticInfo() {
        return new Dataset.DmAttribute.CsticInfo();
    }

    /**
     * Create an instance of {@link NodeStruct.Cref }
     * 
     */
    public NodeStruct.Cref createNodeStructCref() {
        return new NodeStruct.Cref();
    }

    /**
     * Create an instance of {@link Dataset.Characteristics.Cstic }
     * 
     */
    public Dataset.Characteristics.Cstic createDatasetCharacteristicsCstic() {
        return new Dataset.Characteristics.Cstic();
    }

}
