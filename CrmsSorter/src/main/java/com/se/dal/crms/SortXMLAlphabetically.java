package com.se.dal.crms;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.se.dal.crms.jaxb.Dataset;
import com.se.dal.crms.jaxb.NodeStruct;
import com.se.dal.crms.jaxb.Dataset.DmValuation;
import com.se.dal.crms.jaxb.Dataset.Renditions;

public class SortXMLAlphabetically {

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	private final File xmlFile;
	private Dataset dataset;
	private JAXBContext jaxbContext;
	private String outputFileName;
	
	public SortXMLAlphabetically(File xmlFile, String outputFileName) throws Exception {
		this.xmlFile = xmlFile;
		this.outputFileName = outputFileName;
		loadDataSet(xmlFile);
	}
	
	private void loadDataSet(File xmlFile) throws IOException, JAXBException {
		//delete xml header to unmarshal otherwise failed
		InputStream content = deleteXMLheader(xmlFile);
		jaxbContext = JAXBContext.newInstance("com.se.dal.crms.jaxb");
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		dataset = (Dataset) unmarshaller.unmarshal(content);
	}
	
	/**
	 * Sort XML data with specific order (see comparators for each Block)
	 * 
	 * @throws Exception
	 */
	public void sort() throws Exception {
		
		List<Dataset.Characteristics.Cstic> csticList = dataset.getCharacteristics().getCstic();
		List<NodeStruct> nodeStructList = dataset.getNodeStruct().getNodeStruct();
		List<Dataset.DmAttribute.CsticInfo> csticInfoList = dataset.getDmAttribute().getCsticInfo();
		List<Dataset.DmValuation> dmValuationList  = dataset.getDmValuation();
		List<Renditions.MatRendition> matRenditionList = dataset.getRenditions().getMatRendition();
		List<Renditions.CsticRendition> csticRenditionList = dataset.getRenditions().getCsticRendition();
		List<Renditions.CsticValueRendition> csticValueRenditionList = dataset.getRenditions().getCsticValueRendition();
		
		//get number of elements in each list before sorting elements
		int nb_csticList = csticList.size();
		int nb_nodeStructList = nodeStructList.size();
		int nb_csticInfoList = csticInfoList.size();
		int nb_dmValuationList = dmValuationList.size();
		int nb_matRenditionList = matRenditionList.size();
		int nb_csticRenditionList = csticRenditionList.size();
		int nb_csticValueRenditionList = csticValueRenditionList.size();
		
		LOGGER.info("Number of Cstic node(s): " + nb_csticList);
		LOGGER.info("Number of NodeStruct node(s): " + nb_nodeStructList);
		LOGGER.info("Number of CsticInfo node(s): " + nb_csticInfoList);
		LOGGER.info("Number of DmValuation node(s): " + nb_dmValuationList);
		LOGGER.info("Number of MatRendition node(s): " + nb_matRenditionList);
		LOGGER.info("Number of CsticRendition node(s): " + nb_csticRenditionList);
		LOGGER.info("Number of CsticValueRendition node(s): " + nb_csticValueRenditionList);
		
		sortCsticBlock(dataset, csticList);
		sortNodeStructBlock(dataset, nodeStructList);
		sortCsticInfoBlock(dataset, csticInfoList);
		sortDmValuationBlock(dataset, dmValuationList);
		sortMatRenditionBlock(dataset, matRenditionList);
		sortCsticRenditionBlock(dataset, csticRenditionList);
		sortCsticRenditionValueBlock(dataset, csticValueRenditionList);
		
		assertTrue("The sorted cstic list is wrong", nb_csticList == dataset.getCharacteristics().getCstic().size());
		assertTrue("The sorted nodeStruct list is wrong", nb_nodeStructList == dataset.getNodeStruct().getNodeStruct().size());
		assertTrue("The sorted csticInfo list is wrong", nb_csticInfoList == dataset.getDmAttribute().getCsticInfo().size());
		assertTrue("The sorted dmValuation list is wrong", nb_dmValuationList == dataset.getDmValuation().size());
		assertTrue("The sorted matRendition list is wrong", nb_matRenditionList == dataset.getRenditions().getMatRendition().size());
		assertTrue("The sorted csticRendition list is wrong", nb_csticRenditionList == dataset.getRenditions().getCsticRendition().size());
		assertTrue("The sorted csticValueRendition list is wrong", nb_csticValueRenditionList == dataset.getRenditions().getCsticValueRendition().size());
		
		//generate sorted xml file
		createOutputFile();
	}
	
	
	private void createOutputFile() throws Exception {
		String outputDir = xmlFile.getParentFile().getPath();
		LOGGER.info("Creating in directory '{}' the xml file '{}' ", outputDir, outputFileName);
		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.marshal(dataset, new File(outputDir + File.separator + outputFileName));
	}
	
	
	private InputStream deleteXMLheader(File file) throws IOException {
		StringBuilder builder = new StringBuilder();
		try (BufferedReader br = Files.newBufferedReader(file.toPath())) {
			String line;
			int i = 0;
			while ((line = br.readLine()) != null) {
				if(i > 0) {
					builder.append(line);
					builder.append("\n");
				}
				i++;
			}
		}
		//uncomment to see the modified stream
		//LOGGER.debug(builder.toString());
		return new ByteArrayInputStream(builder.toString().getBytes(StandardCharsets.UTF_8));
	}
	
	private static void sortCsticBlock(Dataset dataset, List<Dataset.Characteristics.Cstic> csticList) {
		//sort alphabetically cstic list (ascending order)
		Comparator<Dataset.Characteristics.Cstic> byCsticId = (e1, e2) -> e1.getCsticId().compareTo(e2.getCsticId());
		List<Dataset.Characteristics.Cstic> csticSortedList = csticList.stream().sorted(byCsticId).collect(Collectors.toList());
		//delete the previous list
		dataset.getCharacteristics().getCstic().clear();
		dataset.getCharacteristics().getCstic().addAll(csticSortedList);
	}
	
	private static void sortNodeStructBlock(Dataset dataset, List<NodeStruct> nodeStructList) {
		//sort alphabetically nodeStruct list(ascending order)
		Comparator<NodeStruct> byNodeId = (e1, e2) -> e1.getNodeId().compareTo(e2.getNodeId());
		List<NodeStruct> nodeStructSortedList = nodeStructList.stream().sorted(byNodeId).collect(Collectors.toList());
		// sort Cref inside NodeStruct
		Comparator<NodeStruct.Cref> byCref = (e1, e2) -> e1.getCref().compareTo(e2.getCref());
		for(NodeStruct ns: nodeStructSortedList) {
			if(CollectionUtils.isNotEmpty(ns.getCref())) {
				List<NodeStruct.Cref> crefSortedList = ns.getCref().stream().sorted(byCref).collect(Collectors.toList());
				ns.getCref().clear();
				ns.getCref().addAll(crefSortedList);
			}
		}
		
		//delete the previous list
		dataset.getNodeStruct().getNodeStruct().clear();
		dataset.getNodeStruct().getNodeStruct().addAll(nodeStructSortedList);
	}
	
	private static void sortCsticInfoBlock(Dataset dataset, List<Dataset.DmAttribute.CsticInfo> csticInfoList) {
		//sort alphabetically CsticInfo list(ascending order)
		Comparator<Dataset.DmAttribute.CsticInfo> byDmId_info = (e1, e2) -> e1.getDmId().compareTo(e2.getDmId());
		Comparator<Dataset.DmAttribute.CsticInfo> byCsticId_info = (e1, e2) -> e1.getCsticId().compareTo(e2.getCsticId());
		Comparator<Dataset.DmAttribute.CsticInfo> byPos = (e1, e2) -> e1.getPos().compareTo(e2.getPos());
		List<Dataset.DmAttribute.CsticInfo> csticInfoSortedList = csticInfoList.stream().
				sorted(byDmId_info.thenComparing(byCsticId_info).thenComparing(byPos)).collect(Collectors.toList());
		//delete the previous list
		dataset.getDmAttribute().getCsticInfo().clear();
		dataset.getDmAttribute().getCsticInfo().addAll(csticInfoSortedList);
	}
	
	private static void sortDmValuationBlock(Dataset dataset, List<Dataset.DmValuation> dmValuationList) {
		//sort alphabetically dmValuation list(ascending order)
		Comparator<Dataset.DmValuation> byDmId_val = (e1, e2) -> e1.getDmId().compareTo(e2.getDmId());
		List<Dataset.DmValuation> dmValuationSortedList = dmValuationList.stream().sorted(byDmId_val).collect(Collectors.toList());
			
		// sorted alphabetically dm_valuation by Cref and all sublists(CrefValuation and CsticValue) in ascending order
		Comparator<DmValuation.CrefValuation> byCref_val = (e1, e2) -> e1.getCref().compareTo(e2.getCref());
		// sorted by cstic_id next val_id and Row on dm_valuation/cref_valuation/cstic_value
		Comparator<DmValuation.CrefValuation.CsticValue> byCrefCstic_id = (e1, e2) -> e1.getCsticId().compareTo(e2.getCsticId());
		Comparator<DmValuation.CrefValuation.CsticValue> byCrefCstic_valId = (e1, e2) -> e1.getValId().compareTo(e2.getValId());
		Comparator<DmValuation.CrefValuation.CsticValue> byCrefCstic_row = (e1, e2) -> e1.getRow().compareTo(e2.getRow());
		for(Dataset.DmValuation dmValuation: dmValuationSortedList) {
			if(CollectionUtils.isNotEmpty(dmValuation.getCrefValuation())) {
				List<DmValuation.CrefValuation> crefValSortedList = dmValuation.getCrefValuation().stream().sorted(byCref_val).collect(Collectors.toList());
				for(DmValuation.CrefValuation crefValuation : crefValSortedList) {
					if(CollectionUtils.isNotEmpty(crefValuation.getCsticValue())){
						List<DmValuation.CrefValuation.CsticValue> crefValCsticSortedList = crefValuation.getCsticValue().stream()
								.sorted(byCrefCstic_id.thenComparing(byCrefCstic_valId).thenComparing(byCrefCstic_row))
								.collect(Collectors.toList());
						crefValuation.getCsticValue().clear();
						crefValuation.getCsticValue().addAll(crefValCsticSortedList);
					}
				}
				
				dmValuation.getCrefValuation().clear();
				dmValuation.getCrefValuation().addAll(crefValSortedList);
			}
		}
		
		//delete the previous list
		dataset.getDmValuation().clear();
		dataset.getDmValuation().addAll(dmValuationSortedList);
	}
	
	private static void sortMatRenditionBlock(Dataset dataset, List<Dataset.Renditions.MatRendition> matRenditionList) {
		//sort alphabetically matRendition list(ascending order)
		Comparator<Renditions.MatRendition> byMatId = (e1, e2) -> e1.getMatId().compareTo(e2.getMatId());
		List<Renditions.MatRendition> matRenditionSortedList = matRenditionList.stream().
				sorted(byMatId).collect(Collectors.toList());
		//delete the previous list
		dataset.getRenditions().getMatRendition().clear();
		dataset.getRenditions().getMatRendition().addAll(matRenditionSortedList);
	}
	
	private static void sortCsticRenditionBlock(Dataset dataset, List<Dataset.Renditions.CsticRendition> csticRenditionList) {
		//sort alphabetically csticRendition list(ascending order)
		Comparator<Renditions.CsticRendition> byCsticId_rend = (e1, e2) -> e1.getCsticId().compareTo(e2.getCsticId());
		List<Renditions.CsticRendition> csticRenditionSortedList = csticRenditionList.stream()
				.sorted(byCsticId_rend).collect(Collectors.toList());
		//delete the previous list
		dataset.getRenditions().getCsticRendition().clear();
		dataset.getRenditions().getCsticRendition().addAll(csticRenditionSortedList);
	}
	
	private static void sortCsticRenditionValueBlock(Dataset dataset, List<Dataset.Renditions.CsticValueRendition> csticValueRenditionList) {
		//sort alphabetically csticValueRendition list(ascending order)
		Comparator<Renditions.CsticValueRendition> byCsticId_valRend = (e1, e2) -> e1.getCsticId().compareTo(e2.getCsticId());
		Comparator<Renditions.CsticValueRendition> byValId = (e1, e2) -> e1.getValId().compareTo(e2.getValId());
		List<Renditions.CsticValueRendition> csticValueRenditionSortedList = csticValueRenditionList.stream()
				.sorted(byCsticId_valRend.thenComparing(byValId)).collect(Collectors.toList());
		//delete the previous list
		dataset.getRenditions().getCsticValueRendition().clear();
		dataset.getRenditions().getCsticValueRendition().addAll(csticValueRenditionSortedList);
	}
}
