//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.11 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2021.01.25 à 05:53:10 PM CET 
//


package com.se.dal.crms.jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Classe Java pour node_struct complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="node_struct"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="cref" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                 &lt;attribute name="cref" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="dm_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="node_struct" type="{}node_struct" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="step_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="node_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="node_name" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="node_type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="rel_type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "node_struct", propOrder = {
    "cref",
    "nodeStruct"
})
public class NodeStruct {

    protected List<NodeStruct.Cref> cref;
    @XmlElement(name = "node_struct")
    protected List<NodeStruct> nodeStruct;
    @XmlAttribute(name = "step_id")
    protected String stepId;
    @XmlAttribute(name = "node_id")
    protected String nodeId;
    @XmlAttribute(name = "node_name")
    protected String nodeName;
    @XmlAttribute(name = "node_type")
    protected String nodeType;
    @XmlAttribute(name = "rel_type")
    protected String relType;

    /**
     * Gets the value of the cref property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cref property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCref().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NodeStruct.Cref }
     * 
     * 
     */
    public List<NodeStruct.Cref> getCref() {
        if (cref == null) {
            cref = new ArrayList<NodeStruct.Cref>();
        }
        return this.cref;
    }

    /**
     * Gets the value of the nodeStruct property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nodeStruct property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNodeStruct().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NodeStruct }
     * 
     * 
     */
    public List<NodeStruct> getNodeStruct() {
        if (nodeStruct == null) {
            nodeStruct = new ArrayList<NodeStruct>();
        }
        return this.nodeStruct;
    }

    /**
     * Obtient la valeur de la propriété stepId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStepId() {
        return stepId;
    }

    /**
     * Définit la valeur de la propriété stepId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStepId(String value) {
        this.stepId = value;
    }

    /**
     * Obtient la valeur de la propriété nodeId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNodeId() {
        return nodeId;
    }

    /**
     * Définit la valeur de la propriété nodeId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNodeId(String value) {
        this.nodeId = value;
    }

    /**
     * Obtient la valeur de la propriété nodeName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNodeName() {
        return nodeName;
    }

    /**
     * Définit la valeur de la propriété nodeName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNodeName(String value) {
        this.nodeName = value;
    }

    /**
     * Obtient la valeur de la propriété nodeType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNodeType() {
        return nodeType;
    }

    /**
     * Définit la valeur de la propriété nodeType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNodeType(String value) {
        this.nodeType = value;
    }

    /**
     * Obtient la valeur de la propriété relType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelType() {
        return relType;
    }

    /**
     * Définit la valeur de la propriété relType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelType(String value) {
        this.relType = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *       &lt;attribute name="cref" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="dm_id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class Cref {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "cref")
        protected String cref;
        @XmlAttribute(name = "dm_id")
        protected String dmId;

        /**
         * Obtient la valeur de la propriété value.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Définit la valeur de la propriété value.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Obtient la valeur de la propriété cref.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCref() {
            return cref;
        }

        /**
         * Définit la valeur de la propriété cref.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCref(String value) {
            this.cref = value;
        }

        /**
         * Obtient la valeur de la propriété dmId.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDmId() {
            return dmId;
        }

        /**
         * Définit la valeur de la propriété dmId.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDmId(String value) {
            this.dmId = value;
        }

    }

}
