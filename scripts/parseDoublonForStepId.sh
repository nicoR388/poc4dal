#!/bin/sh
# identify JSON files with many identical stepId

DIR_TO_PARSE=$1
WORKING_DIR="apc-working"

echo "Create directory '$WORKING_DIR'"
mkdir "./$WORKING_DIR"
cd  "./$WORKING_DIR"

echo "Parse all JSON files in '$DIR_TO_PARSE'"
for f in "$DIR_TO_PARSE"/*.json; do

  grep "stepId" "$f" | awk -F":" '{print $2}' | sort | tr ',' ' ' > stepId.log
  cat stepId.log | uniq > uniqueStepId.log
  diff --ignore-all-space --ignore-blank-lines stepId.log uniqueStepId.log | grep '<' | awk '{print $2}' > diffStepId.log
  NBLINES=$(cat diffStepId.log | wc -l)
  FILENAME=$(basename "$f")
  #echo "number of line: $NBLINES"

  echo ""
  if [ "$NBLINES" -eq 0 ]; then
    echo "> No doublon. File '$FILENAME' is ok"
  else
    echo "> StepId in doublon in file '$FILENAME'"
    # call uniq again in case of N identical stepId in the diff
    cat diffStepId.log | sort | uniq
  fi

done

echo ""
echo "Delete working direcory '$WORKING_DIR'"
rm -rf "./$WORKING_DIR"

echo "End Program"
