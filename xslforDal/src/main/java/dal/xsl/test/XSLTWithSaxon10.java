/**
 *  Copyright 2005-2015 Red Hat, Inc.
 *
 *  Red Hat licenses this file to you under the Apache License, version
 *  2.0 (the "License"); you may not use this file except in compliance
 *  with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied.  See the License for the specific language governing
 *  permissions and limitations under the License.
 */
package dal.xsl.test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.time.Instant;

import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;

import org.xml.sax.InputSource;

import net.sf.saxon.s9api.DocumentBuilder;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.XdmAtomicValue;
import net.sf.saxon.s9api.XdmNode;
import net.sf.saxon.s9api.XsltTransformer;

/**
 * Example of the "main class". Put your bootstrap logic here.
 */
public class XSLTWithSaxon10
{
  public static void main(String[] args)
	throws IOException, SaxonApiException	   
	{
	  String output = "src/main/resources/result/output2.xml";
	  String xslPath = "src/main/resources/xsl/google2.xsl";
	  String inputdataAsString = "<rss>\r\n" + 
	  		"    <channel>\r\n" + 
	  		"        <title>APC Product Feed</title>\r\n" + 
	  		"        <link>http://empire.apc.com;</link>\r\n" + 
	  		"        <description>APC Product feed for UNITED STATES .Generated for Google. Number of Products:900 Date Generated:2019-08-20T00:09:16.550+02:00</description>\r\n" + 
	  		"        <item>\r\n" + 
	  		"            <title>APC Performance SurgeArrest 8 Outlet 120V</title>\r\n" + 
	  		"            <upc>731304336594</upc>\r\n" + 
	  		"            <link>https://www.apc.com/shop/us/en/products/APC-Performance-SurgeArrest-8-Outlet-120V/P-P8</link>\r\n" + 
	  		"            <description>Maximum Power Surge Protection for Computers, Notebooks and Other Electronics</description>\r\n" + 
	  		"            <id>P8</id>\r\n" + 
	  		"            <product_type>Power &gt; Surge Protection and Power Conditioning &gt; Surge Protection Devices &gt; SurgeArrest Performance</product_type>\r\n" + 
	  		"            <weight>1.37 lbs</weight>\r\n" + 
	  		"            <image_link>https://www.apc.com/resource/images/salestools/500/Front_Left/F9058DF2418A72D4032581F3005DEA98_NCAO_ATXNB9_f_v_500x500.jpg</image_link>\r\n" + 
	  		"            <price>31.0 USD</price>\r\n" + 
	  		"            <brand>APC</brand>\r\n" + 
	  		"            <mpn>P8</mpn>\r\n" + 
	  		"            <availability>in stock</availability>\r\n" + 
	  		"            <condition>new</condition>\r\n" + 
	  		"        </item>\r\n" + 
	  		"        <item>\r\n" + 
	  		"            <title>APC Network SurgeArrest, 8 outlet</title>\r\n" + 
	  		"            <upc>731304000846</upc>\r\n" + 
	  		"            <link>https://www.apc.com/shop/us/en/products/APC-Network-SurgeArrest-8-outlet/P-NET8</link>\r\n" + 
	  		"            <description>Maximum Power Surge Protection for Computers, Notebooks and Other Electronics</description>\r\n" + 
	  		"            <id>NET8</id>\r\n" + 
	  		"            <product_type>Power &gt; Surge Protection and Power Conditioning &gt; Surge Protection Devices &gt; SurgeArrest Performance</product_type>\r\n" + 
	  		"            <weight>1.9 lbs</weight>\r\n" + 
	  		"            <image_link>https://www.apc.com/resource/images/500/Front_Left/500C8740-5056-AE36-FEEB2C8305680E62_pr.jpg</image_link>\r\n" + 
	  		"            <price>31.0 USD</price>\r\n" + 
	  		"            <brand>APC</brand>\r\n" + 
	  		"            <mpn>NET8</mpn>\r\n" + 
	  		"            <availability>in stock</availability>\r\n" + 
	  		"            <condition>new</condition>\r\n" + 
	  		"        </item>\r\n" + 
	  		"	</channel>\r\n" + 
	  		"</rss>";
	  
	    long timeStampMillisDep = Instant.now().toEpochMilli();	
	  
	  	// Saxon is a powerful XSL engine (manage XSLT 1.0, 2.0 and 3.0). Below we use XSLT 2.0
	    // Saxon HE (Home Edition) is open source and free
	    Processor processor = new Processor(false);
	    StreamSource stylesheet = new StreamSource(new FileInputStream(xslPath));
		XsltTransformer transformer = processor.newXsltCompiler().compile(stylesheet).load();
		transformer.setParameter(new QName("gXmlns"), new XdmAtomicValue("http://base.google.com/ns/1.0"));
		transformer.setParameter(new QName("cXmlns"), new XdmAtomicValue("http://base.google.com/ns/1.0"));
		transformer.setParameter(new QName("version"), new XdmAtomicValue("1.0"));
		DocumentBuilder builder = processor.newDocumentBuilder();
		//small InputData
		//context is the root node
//		InputStream inStream = new ByteArrayInputStream(inputdataAsString.getBytes(StandardCharsets.UTF_8));
//		XdmNode context = builder.build(new StreamSource(inStream));
		
		// Uncomment if necessary to test big input data
	    // you must comment the small inputData part!!!
	    File bigInputFile = new File ("src/main/resources/bigInput.xml");
	    XdmNode context = builder.build(new StreamSource(new FileInputStream(bigInputFile)));
		
		transformer.setInitialContextNode(context);
		Serializer destination = processor.newSerializer(new File(output));
		transformer.setDestination(destination);
		System.out.println("Generating output file: " + output);
		transformer.transform();
	    
	    long timeStampMillisEnd = Instant.now().toEpochMilli();
	    System.out.println("Computing time: " + (timeStampMillisEnd - timeStampMillisDep) + " ms");
  }
}
