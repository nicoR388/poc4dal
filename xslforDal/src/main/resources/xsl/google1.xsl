<!-- XSLT 1.0 -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--if you don't need the xml header set up omit-xml-declaration="yes" -->
 <xsl:output method="xml" version="1.0" encoding="UTF-8" standalone="yes" indent="yes"/>
 
 <!-- For parameter with default value -->
<!--  <xsl:param name="version" select="'default value'"/> -->
 <xsl:param name="version"/>
 <xsl:param name="gXmlns"/>
 
 <!-- copy all nodes with attribute -->
 <xsl:template match="node()|@*">
  <xsl:copy>
   <xsl:apply-templates select="node()|@*"/>
  </xsl:copy>
 </xsl:template>

 <!-- add static data (attributes, namespaces) for rss root node -->
 <xsl:template match="/rss">
   <g:rss version="1.0" xmlns:g="http://base.google.com/ns/1.0" xmlns:c="http://base.google.com/ns/1.0">
   		<xsl:apply-templates />
   </g:rss>
 </xsl:template>
 
 
 <!-- Uncomment if you want dynamic namespace -->
  <!-- add dynamic data (attributes, namespace (only one)) for rss root node -->
<!--  the namespace URI enclosed in curly braces {} -->
<!-- Be careful in XSLT 1.0 is not possible to add multiple dynamic namespaces on a given node -->
<!--  <xsl:template match="/rss"> -->
<!--  	<xsl:element name="g:rss" namespace="{$gXmlns}"> -->
<!--  	  <xsl:attribute name="version"> -->
<!--          <xsl:value-of select="$version"/> -->
<!--       </xsl:attribute> -->
<!--       <xsl:apply-templates /> -->
<!--     </xsl:element> -->
<!--  </xsl:template> -->


 <!-- add prefix p: to all child elements of item elements -->
 <xsl:template match="channel/item/*">
  <xsl:element name="g:{local-name()}" namespace="{$gXmlns}">
   <xsl:value-of select="node()"/>
   <xsl:apply-templates select="*"/>
  </xsl:element>
 </xsl:template>
 
</xsl:stylesheet>