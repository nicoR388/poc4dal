<!-- XSLT 2.0 -->
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--if you don't need the xml header set up omit-xml-declaration="yes" -->
 <xsl:output method="xml" version="1.0" encoding="UTF-8" standalone="yes" indent="yes"/>
 
 <!-- For parameter with default value -->
<!--  <xsl:param name="version" select="'default value'"/> -->
 <xsl:param name="version"/>
 <xsl:param name="gXmlns"/>
 <xsl:param name="cXmlns"/>
 
 <!-- copy all nodes with attribute -->
 <xsl:template match="node()|@*">
  <xsl:copy>
   <xsl:apply-templates select="node()|@*"/>
  </xsl:copy>
 </xsl:template>

 <!-- add specific data (attributes, namespaces) for rss root node -->
 <!--  the namespace URI enclosed in curly braces {} -->
 <xsl:template match="/rss">
  <xsl:element name="g:{local-name()}" namespace="{$gXmlns}">
   <xsl:attribute name="version" select="$version"/>
   <xsl:namespace name="c" select="$cXmlns"/>
   <xsl:apply-templates />
  </xsl:element>
 </xsl:template>

 <!-- add prefix p: to all child elements of item elements -->
 <xsl:template match="channel/item/*">
  <xsl:element name="g:{local-name()}" namespace="{$gXmlns}">
   <xsl:value-of select="node()"/>
   <xsl:apply-templates select="*"/>
  </xsl:element>
 </xsl:template>
 
</xsl:stylesheet>